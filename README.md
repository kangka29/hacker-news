# Hacker News Clone

This app is a clone of [Hacker News](https://news.ycombinator.com/).
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

### `npm run lint`

Checks your scripts and styles for lint errors.

### `npm run beautify`

Beautifies your code using prettier.

### `npm run lint-fix`

Beautifies code and fixes lint errors.

## Features

### Typescript

Type safety with static typing

### SASS

Sass support added

### Material UI & Google Charts

Beautiful responsive design

### Redux

State management for Model & View

### CI/CD ready

Continuous Integration and Continuous Deployment is enabled with Bitbucket Pipelines and site is deployed to Heroku

## Routes

### /

Takes you to homepage

### /news

Opens news panel, add query parameter `page=<page no>` for subsequent pages

### /error

Redirected in case of error

### default

Nowhere

## Limitations

### No support for login

User login not supported as in original website, this is as required.

### Hide row

In original website, a user is required to login first before hiding a row.
When a row is hidden, an API request is made to fetch next available row for user.

Since user login is not available in this clone, fetching the next available row is not feasible. Additionally, there is no API available to do the same.

Hence, on a particular page, if user hides all the rows, they will end up with empty table.

### Bookmarks

Bookmarks for different pages of news will display the current available data from the API, rather than the data available at the time of bookmark.

### Junk data in API response

Records without title have been omitted.
Hence, some pages will have lesser records.
