export const apiBaseUrl = `https://hn.algolia.com/api/v1/`

export const getRandomNumber = (min: number, max: number): number =>
  Math.random() * (max - min) + min

export const getRandomInteger = (min: number, max: number): number => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}
