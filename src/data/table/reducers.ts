import { combineReducers } from 'redux'
import {
  TableActionTypes,
  RawTableData,
  FormattedTableData,
  LocalData,
} from './actions'
import { AppAction, AsyncStatus } from '../../actions'

export interface TableStore {
  rawTableData: RawTableData | null
  formattedTableData: FormattedTableData | null
  localData: LocalData | null
  loadProgress: AsyncStatus | undefined
}

const formattedTableData = (
  state: FormattedTableData | null = null,
  action: AppAction<FormattedTableData>
): FormattedTableData | null => {
  switch (action.type) {
    case TableActionTypes.GetFormattedData:
      return action.payload
    default:
      return state
  }
}

const rawTableData = (
  state: RawTableData | null = null,
  action: AppAction<RawTableData>
): RawTableData | null => {
  switch (action.type) {
    case TableActionTypes.GetRawData:
      return action.payload
    default:
      return state
  }
}

const localData = (
  state: LocalData | null = null,
  action: AppAction<LocalData>
): LocalData | null => {
  switch (action.type) {
    case TableActionTypes.SetLocalData:
      return action.payload
    default:
      return state
  }
}

const loadProgress = (
  state: AsyncStatus = AsyncStatus.none,
  action: AppAction<AsyncStatus>
): AsyncStatus | undefined => {
  switch (action.type) {
    case TableActionTypes.LoadProgress:
      return action.status
    default:
      return state
  }
}

export const tableReducers = combineReducers<TableStore>({
  rawTableData,
  formattedTableData,
  localData,
  loadProgress,
})
