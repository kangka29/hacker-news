import axios, { AxiosResponse } from 'axios'
import moment from 'moment'
import { Dispatch } from 'redux'
import { AppAction, AsyncStatus } from '../../actions'
import { ThunkActionAsync, ThunkActionSync, AppStore } from '../../reducers'
import { apiBaseUrl, getRandomInteger } from '../../util'

export enum TableActionTypes {
  GetRawData = 'GetRawData',
  GetFormattedData = 'GetFormattedData',
  SetLocalData = 'SetLocalData',
  LoadProgress = 'LoadProgress',
}

export enum MatchLevel {
  none = 'none',
  full = 'full',
}

export interface HighlightResult {
  value: string
  matchLevel: MatchLevel
  matchedWords: string[]
}

export interface RawTableRow {
  created_at: string
  title: string
  url: string
  author: string
  points: number
  story_text: string | null
  comment_text: string | null
  num_comments: number | null
  story_id: string | null
  story_title: string | null
  story_url: string | null
  parent_id: string | null
  created_at_i: number
  relevancy_score: number
  _tags: string[]
  objectID: string
  _highlightResult: {
    title: HighlightResult
    url: HighlightResult
    author: HighlightResult
  }
}

export interface RawTableData {
  hits: RawTableRow[]
  nbHits: number
  page: number
  nbPages: number
  hitsPerPage: number
  exhaustiveNbHits: boolean
  query: string
  params: string
  processingTimeMS: number
}

export interface FormattedTableRow {
  id: string
  comments: number
  voteCount: number
  newsDetails: {
    title: string
    url: string
    author: string
    created: string
  }
  hidden: boolean
}

export type FormattedTableData = FormattedTableRow[]

const localStorageItem = {
  modifiedData: `local-news-data`,
}

interface LocalDatabyId {
  voteCount: number
  hidden: boolean
}

export interface LocalData {
  [key: string]: LocalDatabyId
}

const initializeLocalData = (item: string, data: object = {}) => {
  setLocalStorageData(item, data)
  return data
}

// created async function for future compatibility with API interactions
const getInitialLocalData = async (): Promise<LocalData> =>
  getLocalStorageData(localStorageItem.modifiedData) as LocalData

const getLocalStorageData = (item: string): object => {
  let data: object
  try {
    const rawData = localStorage.getItem(item)
    if (rawData) {
      data = JSON.parse(localStorage.getItem(item) as string)
    } else {
      data = initializeLocalData(localStorageItem.modifiedData)
    }
  } catch (e) {
    data = initializeLocalData(localStorageItem.modifiedData)
  }
  return data
}

const setLocalStorageData = (item: string, data: object): void => {
  localStorage.setItem(item, JSON.stringify(data))
}

const setLocalDatabyId = async (
  id: string,
  data: LocalDatabyId
): Promise<void> => {
  const localData: LocalData = await getInitialLocalData()
  localData[id] = data
  setLocalStorageData(localStorageItem.modifiedData, localData)
}

export const formatRowData = async (
  hits: RawTableRow[]
): Promise<FormattedTableData> => {
  const formattedRowData = await hits.reduce(
    async (accPromise: Promise<FormattedTableData>, el: RawTableRow) => {
      const acc = await accPromise
      const {
        created_at,
        num_comments: comments,
        objectID: id,
        title,
        url,
        author,
      } = el
      const createdDuration = moment.duration(moment().diff(moment(created_at)))
      let created: string
      if (Math.floor(createdDuration.asYears()) > 0) {
        created = `${Math.floor(createdDuration.asYears())} year${
          Math.floor(createdDuration.asYears()) > 1 ? `s` : ``
        } ago`
      } else if (Math.floor(createdDuration.asMonths()) > 0) {
        created = `${Math.floor(createdDuration.asMonths())} month${
          Math.floor(createdDuration.asMonths()) > 1 ? `s` : ``
        } ago`
      } else if (Math.floor(createdDuration.asDays()) > 0) {
        created = `${Math.floor(createdDuration.asDays())} day${
          Math.floor(createdDuration.asDays()) > 1 ? `s` : ``
        } ago`
      } else if (Math.floor(createdDuration.asHours()) > 0) {
        created = `${Math.floor(createdDuration.asHours())} hour${
          Math.floor(createdDuration.asHours()) > 1 ? `s` : ``
        } ago`
      } else if (Math.floor(createdDuration.asMinutes()) > 0) {
        created = `${Math.floor(createdDuration.asMinutes())} minute${
          Math.floor(createdDuration.asMinutes()) > 1 ? `s` : ``
        } ago`
      } else {
        created = `just now`
      }
      if (title) {
        const localData = await getInitialLocalData()

        const voteCount = ((localData || {}) as LocalData)[id]
          ? (localData as LocalData)[id].voteCount || 0
          : getRandomInteger(0, 50)
        const hidden = ((localData || {}) as LocalData)[id]
          ? !!(localData as LocalData)[id].hidden
          : false

        if (!hidden) {
          acc.push({
            id,
            comments: comments || 0,
            voteCount,
            newsDetails: {
              title,
              author,
              url,
              created,
            },
            hidden,
          })
        }

        setLocalDatabyId(id, { hidden, voteCount })
      }
      return acc
    },
    Promise.resolve([])
  )
  return formattedRowData
}

export const tableActions = {
  getApiData: (page: number = 0): ThunkActionAsync => {
    return async (dispatch: Dispatch, getState: () => AppStore) => {
      const loadProgress: AppAction<AsyncStatus> = {
        type: TableActionTypes.LoadProgress,
        payload: AsyncStatus.processing,
        status: AsyncStatus.processing,
      }
      dispatch(loadProgress)
      try {
        const apiResponse: AxiosResponse = await axios.get(
          `${apiBaseUrl}search?page=${page}`
        )
        const { data } = apiResponse
        const rawDataAction: AppAction<RawTableData> = {
          type: TableActionTypes.GetRawData,
          payload: data,
        }
        dispatch(rawDataAction)
        const formattedDataAction: AppAction<FormattedTableData> = {
          type: TableActionTypes.GetFormattedData,
          payload: await formatRowData(data.hits),
        }
        dispatch(formattedDataAction)
        loadProgress.payload = AsyncStatus.success
        loadProgress.status = AsyncStatus.success
        dispatch(loadProgress)
      } catch (e) {
        loadProgress.payload = AsyncStatus.error
        loadProgress.status = AsyncStatus.error
        dispatch(loadProgress)
      }
    }
  },
  getInitialLocalData: (): ThunkActionAsync => {
    return async (dispatch: Dispatch) => {
      const localData: AppAction<LocalData> = {
        type: TableActionTypes.SetLocalData,
        payload: await getInitialLocalData(),
      }
      dispatch(localData)
    }
  },
  setLocalData: (id: string, data: LocalDatabyId): ThunkActionSync => {
    return (dispatch: Dispatch, getState: () => AppStore) => {
      const tempLocalData = getState().data.table.localData as LocalData
      const tempFormattedTableData = getState().data.table
        .formattedTableData as FormattedTableData
      tempLocalData[id] = data
      const localData: AppAction<LocalData> = {
        type: TableActionTypes.SetLocalData,
        payload: tempLocalData,
      }
      dispatch(localData)
      setLocalDatabyId(id, data)
      const newFormattedTableData = tempFormattedTableData.reduce(
        (
          acc: FormattedTableData,
          row: FormattedTableRow
        ): FormattedTableData => {
          if (row.id === id) {
            row.voteCount = data.voteCount
            if (!data.hidden) {
              acc.push(row)
            }
          } else {
            acc.push(row)
          }
          return acc
        },
        []
      )
      const formattedDataAction: AppAction<FormattedTableData> = {
        type: TableActionTypes.GetFormattedData,
        payload: newFormattedTableData,
      }
      dispatch(formattedDataAction)
    }
  },
}
