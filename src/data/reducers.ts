import { combineReducers } from 'redux'
import { tableReducers, TableStore } from './table/reducers'

export interface DataStore {
  table: TableStore
}

export const dataReducers = combineReducers<DataStore>({
  table: tableReducers,
})
