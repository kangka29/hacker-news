import { shallow, ShallowWrapper } from 'enzyme'
import { createLocation, createMemoryHistory } from 'history'
import React from 'react'
import { match as routerMatch } from 'react-router'
import configureStore, { MockStoreEnhanced } from 'redux-mock-store'
import thunk from 'redux-thunk'
import {
  formatRowData,
  FormattedTableData,
  LocalData,
} from '../data/table/actions'
import rawTableData from './response.json'

export const shallowRender = (
  Component: any,
  state: {} = {},
  props: {} = {}
): ShallowWrapper => {
  const store = getMockStore(state)
  const wrapper = shallow(<Component store={store} {...props} />)
  expect(wrapper.length).toEqual(1)
  return wrapper
}

export function getMockStore<T = {}>(state: T): MockStoreEnhanced<T> {
  const middleware = [thunk]
  const mockStoreCreator = configureStore<T>(middleware)
  return mockStoreCreator(state)
}

export const mockRawData = (rawTableData as any) as FormattedTableData
export const mockFormattedData = async (): Promise<FormattedTableData> =>
  await formatRowData((mockRawData as any).hits)

type MatchParameter<Params> = { [K in keyof Params]?: string }

export const routerTestProps = <Params extends MatchParameter<Params> = {}>(
  path: string,
  params: Params,
  extendMatch: Partial<routerMatch<any>> = {}
) => {
  const match: routerMatch<Params> = Object.assign(
    {},
    {
      isExact: false,
      path,
      url: generateUrl(path, params),
      params,
    },
    extendMatch
  )
  const history = createMemoryHistory()
  const location = createLocation(match.url)

  return { history, location, match }
}

const generateUrl = <Params extends MatchParameter<Params>>(
  path: string,
  params: Params
): string => {
  let tempPath = path

  for (const param in params) {
    if (params.hasOwnProperty(param)) {
      const value = params[param]
      tempPath = tempPath.replace(
        `:${param}`,
        value as NonNullable<typeof value>
      )
    }
  }

  return tempPath
}
