import { AnyAction, combineReducers } from 'redux'
import {
  ThunkAction as AppThunkAction,
  ThunkDispatch as AppThunkDispatch,
} from 'redux-thunk'
import { dataReducers, DataStore } from './data/reducers'
import { uiReducers, UiStore } from './ui/reducers'

export interface AppStore {
  readonly ui: UiStore
  readonly data: DataStore
}

export const appReducers = combineReducers<AppStore>({
  ui: uiReducers,
  data: dataReducers,
})

export type ThunkDispatch = AppThunkDispatch<AppStore, undefined, AnyAction>
export type ThunkActionAsync = AppThunkAction<
  Promise<void>,
  AppStore,
  undefined,
  AnyAction
>
export type ThunkActionSync = AppThunkAction<
  void,
  AppStore,
  undefined,
  AnyAction
>
