import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.scss'
import { Providers } from './Providers'
import { Error } from './ui/error'
import { Home } from './ui/home'
import { Invalid } from './ui/invalid'
import { NewsWrapper } from './ui/news'

const App = (): JSX.Element => (
  <div className="app-container">
    <Providers>
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/news" exact component={NewsWrapper} />
          <Route path="/error" exact component={Error} />
          <Route component={Invalid} />
        </Switch>
      </Router>
    </Providers>
  </div>
)

export default App
