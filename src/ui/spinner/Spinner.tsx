import { CircularProgress } from '@material-ui/core'
import React from 'react'
import './Spinner.scss'

export const Spinner: React.FunctionComponent = () => {
  return <CircularProgress />
}
