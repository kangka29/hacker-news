import React from 'react'
import './Invalid.scss'

export const Invalid: React.FunctionComponent = () => {
  return (
    <>
      <h1>You are lost</h1>
      <h2>
        Lets take you <a href={`/`}>home</a>
      </h2>
    </>
  )
}
