import React from 'react'
import './Home.scss'

export const Home: React.FunctionComponent = () => {
  return (
    <>
      <h1>Welcome to Hacker News</h1>
      <h2>
        Search for news <a href={`/news`}>here</a>
      </h2>
    </>
  )
}
