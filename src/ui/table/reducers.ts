import { combineReducers } from 'redux'
import { TableActionTypes } from './actions'
import { AppAction } from '../../actions'

export interface TableStore {
  currentPage: number
}

const currentPage = (state: number = 0, action: AppAction<number>): number => {
  switch (action.type) {
    case TableActionTypes.SetCurrentPage:
      return action.payload
    default:
      return state
  }
}

export const tableReducers = combineReducers<TableStore>({
  currentPage,
})
