import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Theme,
  withStyles,
} from '@material-ui/core'
import qs from 'qs'
import React from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import {
  FormattedTableData,
  FormattedTableRow,
  RawTableData,
  tableActions as dataTableActions,
} from '../../data/table/actions'
import { AppStore, ThunkDispatch } from '../../reducers'
import { tableActions } from './actions'
import './Table.scss'

export interface TableDispatchProps {
  setPageNumber: (page: number) => void
  updateRow: (row: FormattedTableRow) => Promise<void>
}

export interface TableStateProps {
  rawTableData: RawTableData | null
  formattedTableData: FormattedTableData | null
}

// tslint:disable-next-line:no-empty-interface
export interface TableOwnProps {}

export type TableProps = TableDispatchProps &
  TableStateProps &
  TableOwnProps &
  RouteComponentProps

// tslint:disable-next-line:no-empty-interface
export interface TableState {}

export interface NewsDetailsCellProps {
  row: FormattedTableRow
  hideMouse: (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => void
  hideEnter: (e: React.KeyboardEvent<HTMLElement>) => void
}

export const NewsDetailsCell: React.FunctionComponent<NewsDetailsCellProps> = (
  props: NewsDetailsCellProps
) => {
  const { row, hideMouse, hideEnter } = props
  const { newsDetails } = row
  const { title, url, author, created } = newsDetails
  // const onClickHide = (
  //   e: React.MouseEvent<HTMLElement, MouseEvent>
  // ): void => {
  //   hide(e)
  // }
  return (
    <div className="news-detail-container">
      <a
        className="news-detail-title news-detail-prominent"
        href={url}
        target="_blank"
        rel="noopener noreferrer"
        tabIndex={0}
      >
        {title}
      </a>
      {author && (
        <>
          <span className="news-detail-fainted">by</span>
          <span className="news-detail-normal">{author}</span>
        </>
      )}
      <span className="news-detail-normal">{created}</span>
      <span
        onClick={hideMouse}
        onKeyPress={hideEnter}
        className="news-detail-hide news-detail-normal"
        tabIndex={0}
        role="link"
      >
        hide
      </span>
    </div>
  )
}

const StyledTableCell = withStyles((theme: Theme) => ({
  head: {
    color: theme.palette.common.white,
    fontFamily: 'Calibri',
    fontSize: '16px',
    fontWeight: 'bold',
    textAlign: 'center',
    padding: '10px',
  },
  body: {
    fontSize: '14px',
    fontFamily: 'Calibri',
    border: 'none',
    textAlign: 'center',
    '&:focus': {
      outline: 'none',
    },
  },
}))(TableCell)

const StyledTable = withStyles((theme: Theme) => ({
  root: {
    width: '100%',
  },
}))(Table)

const StyledTableHead = withStyles((theme: Theme) => ({
  root: {
    backgroundColor: 'orange',
  },
}))(TableHead)

const StyledTableRow = withStyles((theme: Theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    borderBottom: '1px solid rgba(0, 0, 0, 0.2)',
  },
}))(TableRow)

const tableHeaders: string[] = [
  `Comments`,
  `Vote Count`,
  `Upvote`,
  `News Detail`,
]

export class TableWrapped extends React.Component<TableProps, TableState> {
  public componentDidMount(): void {
    window.addEventListener(`popstate`, this.handlePage.bind(this))
  }

  public componentWillUnmount(): void {
    window.addEventListener(`popstate`, this.handlePage.bind(this))
  }

  private handlePage(event: PopStateEvent): void {
    this.props.setPageNumber(
      (qs.parse(document.location.toString().split(`?`)[1], {
        ignoreQueryPrefix: true,
      }) as any).page || 0
    )
  }

  private handleEnterKey(e: React.KeyboardEvent<HTMLElement>): void {
    if (e.type === 'keypress') {
      const keyCode = e.keyCode ? e.keyCode : e.which
      if (keyCode === 13) {
        this.changeLocalData(e)
      }
    }
  }

  private handleClick(e: React.MouseEvent<HTMLElement, MouseEvent>): void {
    this.changeLocalData(e)
  }

  private changeLocalData(
    e:
      | React.MouseEvent<HTMLElement, MouseEvent>
      | React.KeyboardEvent<HTMLElement>
  ): void {
    e.preventDefault()
    e.stopPropagation()

    const context = this as any
    if (context.type === 'vote') {
      context.row.voteCount++
    }
    if (context.type === 'hide') {
      context.row.hidden = true
    }
    context.updateRow(context.row)
  }

  private onChangePage(
    e: React.MouseEvent<HTMLButtonElement, MouseEvent> | null,
    page: number
  ): void {
    this.props.history.push(`/news${!!page ? `?page=${page}` : ``}`)
    this.props.setPageNumber(page)
  }

  public render(): React.ReactNode {
    if (!this.props.formattedTableData) {
      return null
    }

    const rows = this.props.formattedTableData as FormattedTableData

    return (
      <div className="table-container" tabIndex={0}>
        <Paper>
          <TableContainer>
            <StyledTable>
              <StyledTableHead>
                <TableRow>
                  {tableHeaders.map((tableHeader: string, i: number) => (
                    <StyledTableCell key={i}>{tableHeader}</StyledTableCell>
                  ))}
                </TableRow>
              </StyledTableHead>
              {(this.props.formattedTableData || []).length > 0 && (
                <TableBody>
                  {rows.map((row: FormattedTableRow) => (
                    <StyledTableRow key={row.id}>
                      <StyledTableCell>{row.comments}</StyledTableCell>
                      <StyledTableCell>{row.voteCount}</StyledTableCell>
                      <StyledTableCell>
                        <div
                          className="vote"
                          onClick={this.handleClick.bind({
                            row,
                            type: 'vote',
                            updateRow: this.props.updateRow,
                            changeLocalData: this.changeLocalData,
                          })}
                          onKeyPress={this.handleEnterKey.bind({
                            row,
                            type: 'vote',
                            updateRow: this.props.updateRow,
                            changeLocalData: this.changeLocalData,
                          })}
                          tabIndex={0}
                          role="button"
                        />
                      </StyledTableCell>
                      <StyledTableCell>
                        <NewsDetailsCell
                          row={row}
                          hideMouse={this.handleClick.bind({
                            row,
                            type: 'hide',
                            updateRow: this.props.updateRow,
                            changeLocalData: this.changeLocalData,
                          })}
                          hideEnter={this.handleEnterKey.bind({
                            row,
                            type: 'hide',
                            updateRow: this.props.updateRow,
                            changeLocalData: this.changeLocalData,
                          })}
                        />
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              )}
            </StyledTable>
            {(this.props.formattedTableData || []).length === 0 && (
              <StyledTable>
                <StyledTableRow>
                  <StyledTableCell component="div">
                    No records to display
                  </StyledTableCell>
                </StyledTableRow>
              </StyledTable>
            )}
          </TableContainer>
          <TablePagination
            component="div"
            count={
              (this.props.rawTableData?.nbPages || 0) *
              (this.props.rawTableData?.hitsPerPage || 0)
            }
            onChangePage={this.onChangePage.bind(this)}
            page={this.props.rawTableData?.page || 0}
            rowsPerPage={this.props.rawTableData?.hitsPerPage || 0}
            rowsPerPageOptions={[this.props.rawTableData?.hitsPerPage || 0]}
          />
        </Paper>
      </div>
    )
  }
}

const mapStateToProps = (state: AppStore): TableStateProps => ({
  rawTableData: state.data.table.rawTableData,
  formattedTableData: state.data.table.formattedTableData,
})

const mapDispatchToProps = (dispatch: ThunkDispatch): TableDispatchProps => ({
  setPageNumber: (page: number) => {
    dispatch(tableActions.setPageNumber(page))
  },
  updateRow: async (row: FormattedTableRow): Promise<void> => {
    const { id, voteCount, hidden } = row
    await dispatch(dataTableActions.setLocalData(id, { voteCount, hidden }))
  },
})

export const TableWrapper = withRouter(
  connect(mapStateToProps, mapDispatchToProps)(TableWrapped)
)
