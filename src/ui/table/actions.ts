import { ThunkActionSync } from '../../reducers'
import { Dispatch } from 'redux'
import { AppAction } from '../../actions'

export enum TableActionTypes {
  SetCurrentPage = 'SetCurrentPage',
}

export const tableActions = {
  setPageNumber: (page: number): ThunkActionSync => {
    return (dispatch: Dispatch) => {
      const pageNumber: AppAction<number> = {
        type: TableActionTypes.SetCurrentPage,
        payload: page,
      }
      dispatch(pageNumber)
    }
  },
}
