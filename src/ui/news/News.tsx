import qs from 'qs'
import React from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router-dom'
import { AsyncStatus } from '../../actions'
import { FormattedTableData, tableActions } from '../../data/table/actions'
import { AppStore, ThunkDispatch } from '../../reducers'
import { GraphWrapper } from '../graph'
import { Spinner } from '../spinner'
import { TableWrapper } from '../table'
import { tableActions as uiTableActions } from '../table/actions'
import './News.scss'

export interface NewsDispatchProps {
  getInitialLocalData: () => Promise<void>
  getPageData: (page?: number) => Promise<void>
  setPageNumber: (page: number) => void
}

export interface NewsStateProps {
  currentPage: number
  loadProgress: AsyncStatus | undefined
  formattedTableData: FormattedTableData | null
}

// tslint:disable-next-line:no-empty-interface
export interface NewsOwnProps {}

export type NewsProps = NewsDispatchProps &
  NewsStateProps &
  NewsOwnProps &
  RouteComponentProps

// tslint:disable-next-line:no-empty-interface
export interface NewsState {}

export class NewsWrapped extends React.Component<NewsProps, NewsState> {
  private currentPage: number = 0
  public async componentDidMount(): Promise<void> {
    this.currentPage =
      (qs.parse(this.props.location.search, { ignoreQueryPrefix: true }) as any)
        .page || 0
    this.props.setPageNumber(this.currentPage)
    await this.props.getInitialLocalData()
    await this.props.getPageData(this.currentPage)
  }

  public async componentDidUpdate(): Promise<void> {
    if (this.props.currentPage !== this.currentPage) {
      await this.props.getPageData(this.props.currentPage)
      this.currentPage = this.props.currentPage
    }
  }

  public componentDidCatch(): void {
    this.props.history.push(`/error`)
  }

  public render(): React.ReactNode {
    if (this.props.loadProgress === AsyncStatus.processing) {
      return <Spinner />
    }

    return (
      <div className="news-container">
        <TableWrapper />
        {(this.props.formattedTableData || []).length > 0 && <GraphWrapper />}
      </div>
    )
  }
}

const mapStateToProps = (state: AppStore): NewsStateProps => ({
  currentPage: state.ui.table.currentPage,
  loadProgress: state.data.table.loadProgress,
  formattedTableData: state.data.table.formattedTableData,
})

const mapDispatchToProps = (dispatch: ThunkDispatch): NewsDispatchProps => ({
  getInitialLocalData: async (): Promise<void> => {
    await dispatch(tableActions.getInitialLocalData())
  },
  getPageData: async (page: number = 0): Promise<void> => {
    dispatch(tableActions.getApiData(page))
  },
  setPageNumber: (page: number) => {
    dispatch(uiTableActions.setPageNumber(page))
  },
})

export const NewsWrapper = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsWrapped)
