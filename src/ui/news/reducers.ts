import { combineReducers } from 'redux'

// tslint:disable-next-line:no-empty-interface
export interface NewsStore {}

export const newsReducers = combineReducers<NewsStore>({})
