import { NewsWrapped } from '.'
import { AsyncStatus } from '../../actions'
import {
  mockFormattedData,
  routerTestProps,
  shallowRender,
} from '../../test-utils/util'
import { GraphWrapper } from '../graph'
import { TableWrapper } from '../table'
import { NewsProps } from './News'
import { Spinner } from '../spinner'

describe('News', () => {
  const mockProps: NewsProps = {
    getInitialLocalData: jest.fn(),
    getPageData: jest.fn(),
    setPageNumber: jest.fn(),
    currentPage: 0,
    loadProgress: AsyncStatus.none,
    formattedTableData: null,
    ...routerTestProps(`/news`, {}),
  }

  it('renders page', () => {
    const setLocalDataSpy: jest.Mock = jest.fn()
    const apiCallSpy: jest.Mock = jest.fn()
    const props = Object.assign({}, mockProps)
    props.getInitialLocalData = setLocalDataSpy
    props.getPageData = apiCallSpy
    const component = shallowRender(NewsWrapped, {}, props)
    expect(component.exists()).toBe(true)
    expect(setLocalDataSpy).toBeCalledTimes(1)
    setImmediate(() => {
      // wait for promise inside component to be resolved
      expect(apiCallSpy).toBeCalledWith(
        (component.instance() as any).currentPage
      )
    })
  })
  it('renders table', () => {
    const component = shallowRender(NewsWrapped, {}, mockProps)
    expect(component.find(TableWrapper).exists()).toBe(true)
  })
  it('does renders graph when table data is not found', () => {
    const component = shallowRender(NewsWrapped, {}, mockProps)
    expect(component.find(GraphWrapper).exists()).toBe(false)
  })
  fit('renders graph when table data is found', async () => {
    const props = Object.assign({}, mockProps)
    props.formattedTableData = await mockFormattedData()
    const component = shallowRender(NewsWrapped, {}, props)
    expect(component.find(GraphWrapper).exists()).toBe(true)
  })
  it('renders spinner when data loading', () => {
    const props = Object.assign({}, mockProps)
    props.loadProgress = AsyncStatus.processing
    const component = shallowRender(NewsWrapped, {}, props)
    expect(component.find(Spinner).exists()).toBe(true)
  })
})
