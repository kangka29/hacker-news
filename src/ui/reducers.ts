import { combineReducers } from 'redux'
import { tableReducers, TableStore } from './table/reducers'

export interface UiStore {
  table: TableStore
}

export const uiReducers = combineReducers<UiStore>({
  table: tableReducers,
})
