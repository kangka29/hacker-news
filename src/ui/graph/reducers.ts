import { combineReducers } from 'redux'

// tslint:disable-next-line:no-empty-interface
export interface GraphStore {}

export const newsReducers = combineReducers<GraphStore>({})
