import React from 'react'
import Chart from 'react-google-charts'
import { connect } from 'react-redux'
import { FormattedTableData, FormattedTableRow } from '../../data/table/actions'
import { AppStore, ThunkDispatch } from '../../reducers'
import './Graph.scss'

// tslint:disable-next-line:no-empty-interface
export interface GraphDispatchProps {}

export interface GraphStateProps {
  tableData: FormattedTableData | null
}

// tslint:disable-next-line:no-empty-interface
export interface GraphOwnProps {}

export type GraphProps = GraphDispatchProps & GraphStateProps & GraphOwnProps

// tslint:disable-next-line:no-empty-interface
export interface GraphState {}

const titleTextStyle = {
  color: '#000',
  bold: true,
  italic: false,
  fontSize: '20',
  fontName: 'Calibri',
}

const textStyle = {
  color: '#000',
  bold: false,
  italic: false,
  fontSize: '16',
  fontName: 'Calibri',
}

const hAxisTitle = 'ID'
const vAxisTitle = 'Votes'

export class GraphWrapped extends React.Component<GraphProps, GraphState> {
  public render(): React.ReactNode {
    if (!this.props.tableData) {
      return null
    }

    const data = [
      [hAxisTitle, vAxisTitle],
      ...[...this.props.tableData]
        .reverse()
        .map((el: FormattedTableRow) => [el.id, el.voteCount]),
    ]

    return (
      <div className="chart-container" tabIndex={0}>
        <Chart
          width={'100%'}
          height={'500px'}
          chartType="AreaChart"
          data={data}
          options={{
            hAxis: {
              title: hAxisTitle,
              titleTextStyle,
              textStyle,
              direction: -1,
              slantedText: true,
              slantedTextAngle: 90,
            },
            vAxis: {
              title: vAxisTitle,
              titleTextStyle,
              textStyle,
              minValue: 0,
              format: 'short',
            },
            chartArea: { width: '70%', height: '50%' },
            legend: { position: 'none' },
          }}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: AppStore): GraphStateProps => ({
  tableData: state.data.table.formattedTableData,
})

const mapDispatchToProps = (dispatch: ThunkDispatch): GraphDispatchProps => ({})

export const GraphWrapper = connect(
  mapStateToProps,
  mapDispatchToProps
)(GraphWrapped)
