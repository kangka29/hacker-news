import React from 'react'
import './Error.scss'

export const Error: React.FunctionComponent = () => {
  return (
    <>
      <h1>Something went wrong</h1>
      <h2>
        Lets <a href={`/`}>start over</a>
      </h2>
    </>
  )
}
