import { AnyAction } from 'redux'

export enum AsyncStatus {
  none,
  processing,
  success,
  error,
}

export interface AppAction<TPayload> extends AnyAction {
  type: string
  payload: TPayload
  status?: AsyncStatus
}
