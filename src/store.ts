import {
  AnyAction,
  applyMiddleware,
  createStore,
  Store,
  PreloadedState,
} from 'redux'
import { middlewares } from './middlewares'
import { appReducers, AppStore } from './reducers'

export let store: Store | null = null

export const configureStore = (initialState?: PreloadedState<AppStore>) => {
  store = createStore<AppStore, AnyAction, {}, {}>(
    appReducers,
    initialState,
    applyMiddleware(...middlewares)
  )
  return store
}

// export const store = createStore<AppStore, AnyAction, {}, {}>(
//   appReducers,
//   applyMiddleware(...middlewares)
// );
