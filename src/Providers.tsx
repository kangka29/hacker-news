import React from 'react'
import { Provider as ReduxProvider } from 'react-redux'
import { configureStore } from './store'

const store = configureStore()

export const Providers: React.SFC = (props) => (
  <ReduxProvider store={store}>{props.children}</ReduxProvider>
)
