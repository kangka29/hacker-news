import { Middleware } from 'redux'
import thunk from 'redux-thunk'

export const middlewares: Middleware[] = [thunk]
