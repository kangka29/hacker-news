# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2020-07-07

### Added

- Changelog
- Improved accessibility

### Changed

- Vote count in server response is unusually high, hence upvote doesn't noticably make a change in graph. Generate initial vote count locally instead of server response to demonstrate change in graph.

## [1.0.0] - 2020-07-06

### Added

- Initial version
